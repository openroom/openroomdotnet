﻿using System;
using System.Globalization;
using Npgsql;

namespace OpenroomConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var conn = "Server=ruby.db.elephantsql.com;Port=5432;User id=agotgogx;Password=iVsCnSDgtX7n26u-Q42Azg9nXEOo5eIE;Database=agotgogx;";
            NpgsqlConnection objConnection = new NpgsqlConnection(conn);
            objConnection.Open();
            // using (var cmd = new NpgsqlCommand("delete from person", objConnection))
            // {
            //     cmd.ExecuteNonQuery();
            // }
            using (var cmd = new NpgsqlCommand("INSERT INTO person (title) VALUES (@p)", objConnection))
            {
                cmd.Parameters.AddWithValue("p", Environment.UserName);
                cmd.ExecuteNonQuery();
            }
            using (var cmd = new NpgsqlCommand("SELECT title, created_at FROM person", objConnection))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Console.WriteLine(reader.GetString(0));
                        NpgsqlTypes.NpgsqlDateTime x = reader.GetDateTime(1);
                        Console.WriteLine(x.ToUniversalTime().ToString());
                    }
                }
            }
            objConnection.Close();
        }
    }
}
