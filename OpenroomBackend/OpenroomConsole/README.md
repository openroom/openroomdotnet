```sql
drop table person;
create table person
(
  id varchar(255) default gen_random_uuid() primary key,
  title varchar(255) not null,
  created_at timestamp with time zone default current_timestamp
);
```