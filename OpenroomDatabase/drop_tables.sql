show search_path;
select version();
drop table if exists reservations cascade;
drop table if exists room_groups cascade;
drop table if exists rooms cascade;
drop table if exists user_tokens cascade;
drop table if exists users cascade;