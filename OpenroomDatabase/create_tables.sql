ALTER DATABASE nftqdvqc SET timezone TO 'UTC';

create table if not exists users
(
  id uuid default gen_random_uuid(),
  email varchar(255) not null unique,
  password varchar(4096),
  registration_time timestamptz not null,
  is_administrator boolean not null,
  is_reporter boolean not null,
  is_banned boolean not null,
  primary key (id)
);

create table if not exists user_tokens
(
  id uuid default gen_random_uuid(),
  user_id uuid references users not null,
  token varchar(36) default gen_random_uuid() not null,
  primary key (id)
);

create table if not exists room_groups
(
  id uuid default gen_random_uuid(),
  name varchar(255) not null unique,
  minimum_capacity int not null,
  maximum_capacity int not null,
  note text,
  primary key (id)
);

create table if not exists rooms
(
  id uuid default gen_random_uuid(),
  name varchar not null unique,
  minimum_capacity int not null,
  maximum_capacity int not null,
  note text,
  room_group uuid references room_groups,
  primary key (id)
);

create table if not exists reservations
(
  id uuid default gen_random_uuid(),
  start_time timestamptz not null,
  end_time timestamptz not null,
  room_id uuid references rooms,
  user_id uuid references users,
  primary key (id)
);
