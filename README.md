Motivation: 

Openroom.net is an attempt to build Openroom with current technologies: postgresql, asp.net web api, and angular. 

It is not a product that one can use but rather a concept project. 

At its core, a room reservation system is simple. 
We have three main concepts: a person makes a reservation for a room. 

1. person
2. reservation
3. room 

Let us call a room an item as a general idea. 
Multiple items belong to a group: item group. 
Multiple item groups belong to a location: item location. 

Let us count our concepts for items: 

1. Item
2. Item group 
3. Item location 

A reservation is always associated with an item. 
In our make believe world, a reservation does not span multiple items.

So, a reservation is a time slot (with a beginning and an end) that associates a person with an item. 
You may have additional persons associated with a reservation but there is always at least one person associated with a reservation. 

Availability is associated with an item group. 
For the sake of simplicity, an item may only belong to any one group. 
Availability of an item aims to answer the question: assuming there are no conflicting reservations, is this item available to be reserved? 

Open hours is associated with a location. 
A location consists of zero or more item groups. 
Each item group has zero or more items. 
Open hours answers the question: is this location open on this day? 
If so, what time does it open and what time does it close? 

For a reservation request for an item to be confirmed, 

1. a location must be open during the entire time of the reservation, 
2. the item group must be available 
3. the item must not have any conficting reservations 
